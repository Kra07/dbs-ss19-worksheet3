#### Exercise 3.4 
Find all tuples where the grade (of a student) is identical to the ECTS points of the lecture  

```sql
select participates.StudId, Lecture.LId, Grade, ECTS
FROM participates
join Lecture on participates.LId = Lecture.LId
where participates.Grade = Lecture.ECTS ;
```

```
π participates.StudId, Lecture.LId, Grade, ECTS (
  σ participates.Grade = Lecture.ECTS (
    ( participates ) ⨝ participates.LId = Lecture.LId
    ( Lecture )
  )
)
```

| StudId | LId | Grade | ECTS |
| --- | --- | --- | --- |
| 0111111	| 050030 | 2 | 2 |
| 0111111	| 050031 | 3 | 3 |
