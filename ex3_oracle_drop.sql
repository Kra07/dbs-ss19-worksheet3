-- Created by Vertabelo (http://vertabelo.com)
-- Last modification date: 2019-05-12 13:17:09.813

-- views
DROP VIEW R2;

DROP VIEW R1;

-- foreign keys
ALTER TABLE participates
    DROP CONSTRAINT participates_Lecture;

ALTER TABLE participates
    DROP CONSTRAINT participates_Student;

ALTER TABLE studies
    DROP CONSTRAINT studies_DegreeProgram;

ALTER TABLE studies
    DROP CONSTRAINT studies_Student;

-- tables
DROP TABLE DegreeProgram;

DROP TABLE Lecture;

DROP TABLE Student;

DROP TABLE participates;

DROP TABLE studies;

-- End of file.

