#### Exercise 3.3 
Find all students (name and grade) which attended (participated) a lecture in 'ST 2002' and which were graded with a 2, 3 or 4.  

```sql
SELECT Firstname, Lastname, Grade
FROM participates
join Lecture on participates.LId = Lecture.LId
join Student on participates.StudId = Student.StudId
where participates.Semester = 'ST 2002'
and participates.Grade > 1
and participates.Grade < 5 ;
```

```
π Firstname, Lastname, Grade (
  σ participates.Semester = 'ST 2002' (
    σ participates.Grade > 1 (
      σ participates.Grade < 5 (
        ( participates ) ⨝ participates.LId = Lecture.LId
        ( Lecture ) ⨝ participates.StudId = Student.StudId
        ( Student )
      )
    )
  )
)
```

| Student.Firstname | Student.Lastname | participates.Grade |
| --- | --- | -- |
| Martin | Huber	| 2 |
| Martin | Huber	| 3 |
| Johann | Maier	| 4 |
