insert into Lecture ( LId, Name, Type, Hours, ECTS ) values
('050030', 'Datenbanksysteme', 'VO', 1.0, 2.0) ;
insert into Lecture ( LId, Name, Type, Hours, ECTS ) values
('050031', 'Datenbanksysteme', 'UE', 2.0, 3.0) ;
insert into Lecture ( LId, Name, Type, Hours, ECTS ) values
('050054', 'Softwarearchitekturen', 'PR', 2.0, 3.0);
insert into Lecture ( LId, Name, Type, Hours, ECTS ) values
('050056', 'Projektmanagement', 'VU', 4.0, 6.0) ;

insert into Student ( StudId, Firstname, Lastname, Birthday ) values
( '0111111', 'Martin', 'Huber', '1981-01-01' );
insert into Student ( StudId, Firstname, Lastname, Birthday ) values
( '0222222', 'Johann', 'Maier', '1982-05-05' );

insert into DegreeProgram ( Id, Name, Graduation, SumECTS ) values
( '521', 'Informatik', 'Bachelor', '180' );
insert into DegreeProgram ( Id, Name, Graduation, SumECTS ) values
( '526', 'Wirtschaftsinformatik', 'Bachelor', '180' );
insert into DegreeProgram ( Id, Name, Graduation, SumECTS ) values
( '926', 'Wirtschaftsinformatik', 'Master', '120' ) ;
insert into DegreeProgram ( Id, Name, Graduation, SumECTS ) values
( '935', 'Medieninformatik', 'Master', '120' );

insert into participates ( StudId, LId, Semester, Grade ) values
( '0111111', '050030', 'ST 2002', 2 );
insert into participates ( StudId, LId, Semester, Grade ) values
( '0111111', '050031', 'ST 2002', 3 );
insert into participates ( StudId, LId, Semester, Grade ) values
( '0111111', '050056', 'WT 2003', 1 );
insert into participates ( StudId, LId, Semester, Grade ) values
( '0222222', '050054', 'ST 2002', 4 );
insert into participates ( StudId, LId, Semester, Grade ) values
( '0222222', '050056', 'WT 2003', 1 );

insert into studies ( StudId, Id, Since ) values
( '0111111', '521', '2001-10-01' );
insert into studies ( StudId, Id, Since ) values
( '0111111', '926', '2004-03-01' );
insert into studies ( StudId, Id, Since ) values
( '0222222', '521', '2001-10-01' );
insert into studies ( StudId, Id, Since ) values
( '0222222', '526', '2002-03-01' );

