#### Exercise 3.5 
Find the ids and the names of all degree programs (DegreeProgram) which have no students  

```sql
SELECT DegreeProgram.Id, DegreeProgram.Name, COUNT(studies.StudId) as numStud
FROM studies
right JOIN DegreeProgram ON studies.Id = DegreeProgram.Id
group by DegreeProgram.Id, DegreeProgram.Name
having COUNT(studies.StudId) = 0;
```

```
π DegreeProgram.Id, DegreeProgram.Name, numStud (
  σ numStud = 0 (
    γ DegreeProgram.Id, DegreeProgram.Name; COUNT(studies.StudId)→numStud (
      ( studies ) ⟖ studies.Id = DegreeProgram.Id
      ( DegreeProgram )
    )
  )
)
```

| DegreeProgram.Id | DegreeProgram.Name | numStud |
| --- | --- | --- |
| 935 | Medieninformatik | 0 |
