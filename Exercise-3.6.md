#### Exercise 3.6 
Select the names of all students including the number of lectures which they participated in summer term ST 2002  

```sql
SELECT Student.StudId, Student.Firstname, Student.Lastname, COUNT(participates.LId) AS NumLVA
FROM participates
join Student on participates.StudId = Student.StudId
where participates.Semester =  'ST 2002'
GROUP BY Student.StudId, Student.Firstname, Student.Lastname ;
```

```
π Student.StudId, Student.Firstname, Student.Lastname, NumLVA (
	γ Student.StudId, Student.Firstname, Student.Lastname; COUNT(participates.LId)→NumLVA (
		σ participates.Semester = 'ST 2002' (
			( participates ) ⨝ participates.StudId = Student.StudId
			( Student )
		)
	)
)
```

| Student.StudId | Student.Firstname | Student.Lastname | NumLVA |
| --- | --- | --- | --- |
| 0111111	| Martin | Huber | 2 |
| 0222222	| Johann | Maier | 1 |
