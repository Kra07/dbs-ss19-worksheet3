-- Created by Vertabelo (http://vertabelo.com)
-- Last modification date: 2019-05-12 13:17:09.813

-- tables
-- Table: DegreeProgram
CREATE TABLE DegreeProgram (
    Id varchar2(64)  NOT NULL,
    Name varchar2(64)  NOT NULL,
    Graduation varchar2(64)  NOT NULL,
    SumECTS varchar2(64)  NOT NULL,
    CONSTRAINT DegreeProgram_pk PRIMARY KEY (Id)
) ;

-- Table: Lecture
CREATE TABLE Lecture (
    LId varchar2(64)  NOT NULL,
    Name varchar2(64)  NOT NULL,
    Type varchar2(64)  NOT NULL,
    Hours number(10,2)  NOT NULL,
    ECTS number(10,2)  NOT NULL,
    CONSTRAINT Lecture_pk PRIMARY KEY (LId)
) ;

-- Table: Student
CREATE TABLE Student (
    StudId varchar2(64)  NOT NULL,
    Firstname varchar2(64)  NOT NULL,
    Lastname varchar2(64)  NOT NULL,
    Birthday varchar2(64)  NOT NULL,
    CONSTRAINT Student_pk PRIMARY KEY (StudId)
) ;

-- Table: participates
CREATE TABLE participates (
    Semester varchar2(64)  NOT NULL,
    Grade integer  NOT NULL,
    LId varchar2(64)  NOT NULL,
    StudId varchar2(64)  NOT NULL,
    CONSTRAINT participates_pk PRIMARY KEY (Semester,LId,StudId)
) ;

-- Table: studies
CREATE TABLE studies (
    Since varchar2(64)  NOT NULL,
    StudId varchar2(64)  NOT NULL,
    Id varchar2(64)  NOT NULL,
    CONSTRAINT studies_pk PRIMARY KEY (StudId,Id)
) ;

-- views
-- View: R1
CREATE VIEW R1 AS
SELECT *
FROM Student
WHERE Lastname = 'Huber' AND Firstname = 'Martin' ;

-- View: R2
CREATE VIEW R2 AS
SELECT participates.StudId, Name, Semester
FROM participates
join Lecture on participates.LId = Lecture.LId
join R1 on participates.StudId = R1.StudId;

-- foreign keys
-- Reference: participates_Lecture (table: participates)
ALTER TABLE participates ADD CONSTRAINT participates_Lecture
    FOREIGN KEY (LId)
    REFERENCES Lecture (LId);

-- Reference: participates_Student (table: participates)
ALTER TABLE participates ADD CONSTRAINT participates_Student
    FOREIGN KEY (StudId)
    REFERENCES Student (StudId);

-- Reference: studies_DegreeProgram (table: studies)
ALTER TABLE studies ADD CONSTRAINT studies_DegreeProgram
    FOREIGN KEY (Id)
    REFERENCES DegreeProgram (Id);

-- Reference: studies_Student (table: studies)
ALTER TABLE studies ADD CONSTRAINT studies_Student
    FOREIGN KEY (StudId)
    REFERENCES Student (StudId);

-- End of file.

