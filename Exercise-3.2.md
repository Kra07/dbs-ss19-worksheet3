#### Exercise 3.2 
In the following, you find expressions in relational algebra. Find a corresponding expression in SQL and upload the SQL result (table).  
 
```
R1 = σ Firstname = 'Martin' (σ Lastname = 'Huber' (Student))
R2 = π StudId, Name, Semester (Lecture ⨝ participates ⨝ R1)  
```

```sql
CREATE VIEW R1 AS
SELECT *
FROM Student
WHERE Lastname = 'Huber' AND Firstname = 'Martin';
```

| StudId | Firstname | Lastname | Birthday |
| --- | --- | --- | --- |
| 0111111 | Martin | Huber | 1981-01-01 |

```sql
CREATE VIEW R2 AS
SELECT Student.StudId, Lecture.Name, participates.Semester
FROM participates
join Lecture on participates.LId = Lecture.LId
join Student on participates.StudId = Student.StudId;
```

| StudId | Name | Semester |
| --- | --- | --- |
| 0111111 | Datenbanksysteme | ST 2002 |
| 0111111 | Projektmanagement | WT 2003 |