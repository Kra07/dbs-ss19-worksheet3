 
## Worksheet 3: Relational Algebra & SQL-Basics

### Legend

Lecture(LId, Name, Type, Hours, ECTS)  
PK: LId  
     
participates(StudId, LId, Semester, Grade)  
PK: StudId, LId, Semester  
FK: StudId◊ Student.StudId, LId ◊ Lecture.LId  

Student(StudId, Firstname, Lastname, Birthday)  
PK: StudId  
     
studies(StudId, Id, Since)  
PK: StudId, Id  
FK: StudId ◊ Student.StudId, Id ◊ DegreeProgram.Id  
     
DegreeProgram(Id, Name, Graduation, SumECTS)  
PK: Id  

Lecture =  
LId Name Type Hours ECTS  
050030 Datenbanksysteme VO 1,0 2,0  
050031 Datenbanksysteme UE 2,0 3,0  
050054 Softwarearchitekturen PR 2,0 3,0  
050056 Projektmanagement VU 4,0 6,0  
 
participates=  
StudId LId Semester Grade  
0111111 050030 ST 2002 2  
0111111 050031 ST 2002 3  
0111111 050056 WT 2003 1  
0222222 050054 ST 2002 4  
0222222 050056 WT 2003 1  
 
Student =  
StudId Firstname Lastname Birthday  
0111111 Martin Huber 1981-01-01  
0222222 Johann Maier 1982-05-05  
 
studies=  
StudId Id Since  
0111111 521 2001-10-01  
0111111 926 2004-03-01  
0222222 521 2001-10-01  
0222222 526 2002-03-01  
 
DegreeProgram =  
Id Name Graduation SumECTS  
521 Informatik Bachelor 180  
526 Wirtschaftsinformatik Bachelor 180  
926 Wirtschaftsinformatik Master 120  
935 Medieninformatik Master 120 